from django.db import models

class Usuario(models.Model):
    id = models.IntegerField(primary_key=True)
    primer_nombre = models.CharField(max_length=100)
    segundo_nombre = models.CharField(max_length=100, blank=True, null=True)
    primer_apellido = models.CharField(max_length=100)
    segundo_apellido = models.CharField(max_length=100, blank=True, null=True)
    #el email debe ser unico
    email = models.CharField(max_length=100, unique=True)
    contraseña = models.CharField(max_length=100)
    rol = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.primer_nombre + " " + self.primer_apellido

class Equipo(models.Model):
    id = models.IntegerField(primary_key=True)
    tipo = models.CharField(max_length=100)
    sitio = models.CharField(max_length=100)
    voltaje_ope = models.CharField(max_length=100, blank=True, null=True)
    num_compr = models.IntegerField(blank=True, null=True)
    num_blower = models.IntegerField(blank=True, null=True)
    num_venti = models.IntegerField(blank=True, null=True)
    observaciones = models.CharField(max_length=500, blank=True, null=True)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)

    def __str__(self):
        return " " + self.sitio + " " + self.usuario.primer_nombre + " " + self.usuario.primer_apellido

class Mantenimiento(models.Model):
    id = models.IntegerField(primary_key=True)
    tipo_mtto = models.CharField(max_length=100)
    fecha_eje = models.DateField()
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)

    def __str__(self):
        return  " " + self.tipo_mtto + " " + self.usuario.primer_nombre + " " + self.usuario.primer_apellido + " " + self.equipo.tipo + " " + self.equipo.sitio
