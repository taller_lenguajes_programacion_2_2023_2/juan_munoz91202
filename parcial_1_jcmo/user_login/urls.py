from django.urls import path
from . import views


urlpatterns = [    
    path('', views.index),
    path('registro/', views.registro_usuarios),
    path('prueba/', views.prueba),
    path('autenticacion/',views.validar),
    path('PanelControl/',views.panel_control),
    
    
    
]
