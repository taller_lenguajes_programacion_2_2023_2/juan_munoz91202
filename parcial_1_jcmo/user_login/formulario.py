from django import forms

class registro_usuario(forms.Form):
    primer_nombre = forms.CharField(label='Primer nombre', max_length=100)
    segundo_nombre = forms.CharField(label='Segundo nombre', max_length=100)
    primer_apellido = forms.CharField(label='Primer apellido', max_length=100)
    segundo_apellido = forms.CharField(label='Segundo apellido', max_length=100)
    email = forms.EmailField(label='Email', max_length=100)
    contraseña = forms.CharField(label='Contraseña', max_length=100)
    rol = forms.CharField(label='Rol', max_length=100)

class login_usuario(forms.Form):
    email = forms.EmailField(label='Email', max_length=100)
    contraseña = forms.CharField(label='Contraseña', max_length=100)
    
        