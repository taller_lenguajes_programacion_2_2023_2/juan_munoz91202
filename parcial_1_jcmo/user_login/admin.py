from django.contrib import admin
from .models import Usuario, Equipo, Mantenimiento

admin.site.register(Usuario)

admin.site.register(Equipo)

admin.site.register(Mantenimiento)
