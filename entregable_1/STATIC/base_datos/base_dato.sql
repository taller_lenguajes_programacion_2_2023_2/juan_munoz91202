
DROP TABLE equipo CASCADE CONSTRAINTS;

DROP TABLE mantenimiento CASCADE CONSTRAINTS;

DROP TABLE usuario CASCADE CONSTRAINTS;

-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE TABLE equipo (
    id            INTEGER NOT NULL,
    tipo          VARCHAR2(100) NOT NULL,
    sitio         VARCHAR2(100) NOT NULL,
    voltaje_ope   VARCHAR2(100),
    num_compr     INTEGER,
    num_blower    INTEGER,
    num_venti     INTEGER,
    observaciones VARCHAR2(500),
    usuario_id    INTEGER NOT NULL
);

ALTER TABLE equipo ADD CONSTRAINT equipo_pk PRIMARY KEY ( id );

CREATE TABLE mantenimiento (
    id         INTEGER NOT NULL,
    tipo_mtto  VARCHAR2(100) NOT NULL,
    fecha_eje  DATE NOT NULL,
    usuario_id INTEGER NOT NULL,
    equipo_id  INTEGER NOT NULL
);

ALTER TABLE mantenimiento ADD CONSTRAINT mantenimiento_pk PRIMARY KEY ( id );

CREATE TABLE usuario (
    id               INTEGER NOT NULL,
    primer_nombre    VARCHAR2(100) NOT NULL,
    segundo_nombre   VARCHAR2(100),
    primer_apellido  VARCHAR2(100) NOT NULL,
    segundo_apellido VARCHAR2(100),
    email            VARCHAR2(100) NOT NULL,
    contraseña       VARCHAR2(100) NOT NULL,
    rol              VARCHAR2(100)
);

ALTER TABLE usuario ADD CONSTRAINT usuario_pk PRIMARY KEY ( id );

ALTER TABLE mantenimiento
    ADD CONSTRAINT equipo_fk FOREIGN KEY ( equipo_id )
        REFERENCES equipo ( id );

ALTER TABLE mantenimiento
    ADD CONSTRAINT usuario_fk FOREIGN KEY ( usuario_id )
        REFERENCES usuario ( id );

ALTER TABLE equipo
    ADD CONSTRAINT usuario_fkv2 FOREIGN KEY ( usuario_id )
        REFERENCES usuario ( id );

