// Paso 1: Estructura para almacenar datos de usuarios en JSON
var usuarios = [];

// Paso 2: Verificar si ya existe un archivo de usuarios en localStorage
var usuariosGuardados = localStorage.getItem('usuarios');

if (usuariosGuardados) {
    usuarios = JSON.parse(usuariosGuardados);
}

$(entrar).click(function (e) { 
    e.preventDefault();
    window.location.href = "login.html";
    
});

// Evento de clic en el botón de registro
$(Registro).click(function (e) {
    e.preventDefault();

    // Obtener los valores del formulario
    var nombre_1 = $("#Primer_Nombre").val();
    var nombre_2 = $("#Segundo_Nombre").val();
    var apellido_1 = $("#Primer_Apellido").val();
    var apellido_2 = $("#Segundo_Apellido").val();
    var correo = $("#email").val();
    var contraseña = $("#Contraseña").val();
    var contraseña2 = $("#Contraseña2").val();
    var rol = $("#rol").val();
    var empresa = $("#Empresa").val();

    // Verificar si el usuario ya existe
    var usuarioExistente = usuarios.find(function (usuario) {
        return (            
            usuario.correo === correo
        );
    });

    if (usuarioExistente) {
        alert("El usuario ya está registrado.");
    } else {
        if (
            nombre_1 === "" ||
            nombre_2 === "" ||
            apellido_1 === "" ||
            apellido_2 === "" ||
            empresa === "" ||
            correo === "" ||
            contraseña === "" ||
            contraseña2 === "" ||
            rol === "Rol"
        ) {
            alert("Todos los campos son obligatorios");
        } else {
            if (contraseña === contraseña2) {
                // Agregar el nuevo usuario al arreglo
                var nuevoUsuario = {
                    nombre_1: nombre_1,
                    nombre_2: nombre_2,
                    apellido_1: apellido_1,
                    apellido_2: apellido_2,
                    empresa: empresa,
                    correo: correo,
                    contraseña: contraseña,
                    rol: rol,
                    estado:0
                };
                               

                usuarios.push(nuevoUsuario);

                // Actualizar el archivo de usuarios en localStorage
                localStorage.setItem('usuarios', JSON.stringify(usuarios));

                alert("Registro exitoso");

                // Redireccionar al login
                window.location.href = "login.html";

            } else {
                alert("Las contraseñas no coinciden");
            }
        }
    }
});
