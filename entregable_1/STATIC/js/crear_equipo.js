// Version: 1.0
    $(document).ready(function() {
        // Manejador de clic en el botón "Crear"
        $("#Crear").click(function() {
            // Captura los valores de los campos
            var serial = $("#Serie").val();
            var nombreCliente = $("#nombre_cliente").val();
            var capacidadBTU = $("#Capacidad").val();
            var tipoEquipo = $("#Tipo_de_equipo").val();
            var voltaje = $("#Voltaje").val();
            var marca = $("#Marca").val();
            var amperaje = $("#Amperaje").val();
            var modelo = $("#Modelo").val();
            var frecuencia = $("#Frecuencia").val();
            var fase = $("#Fase").val();
            var ubicacion = $("#Ubicacion").val();
            var refrigerante = $("#Refrigerante").val();
            var fechaInstalacion = $("#Fecha_de_instalacion").val();
            var presionBaja = $("#Presion_baja").val();
            var fechaUltimoMantenimiento = $("#Fecha_de_vencimiento").val();
            var presionAlta = $("#Presion_alta").val();

            // Verifica si todos los campos obligatorios están llenos
            if (
                serial === "" ||
                nombreCliente === "" ||
                capacidadBTU === "" ||
                tipoEquipo === "" ||
                voltaje === "" ||
                marca === "" ||
                amperaje === "" ||
                modelo === "" ||
                frecuencia === "" ||
                fase === "" ||
                ubicacion === "" ||
                refrigerante === "" ||
                fechaInstalacion === "" ||
                presionBaja === "" ||
                fechaUltimoMantenimiento === "" ||
                presionAlta === ""
            ) {
                alert("Por favor, complete todos los campos obligatorios.");
            } else {
                // Verifica si ya existe un equipo con el mismo número de serie
                var equiposGuardados = JSON.parse(localStorage.getItem("equipos")) || [];
                var equipoExistente = equiposGuardados.find(function(equipo) {
                    return equipo.serial === serial;
                });

                if (equipoExistente) {
                    alert("El equipo ya está creado.");
                } else {
                    // Crea un objeto con los datos del equipo y establece el estado como "pendiente"
                    var nuevoEquipo = {
                        serial: serial,
                        nombreCliente: nombreCliente,
                        capacidadBTU: capacidadBTU,
                        tipoEquipo: tipoEquipo,
                        voltaje: voltaje,
                        marca: marca,
                        amperaje: amperaje,
                        modelo: modelo,
                        frecuencia: frecuencia,
                        fase: fase,
                        ubicacion: ubicacion,
                        refrigerante: refrigerante,
                        fechaInstalacion: fechaInstalacion,
                        presionBaja: presionBaja,
                        fechaUltimoMantenimiento: fechaUltimoMantenimiento,
                        presionAlta: presionAlta,
                        estado: "pendiente" // Agrega el estado "pendiente"
                    };

                    // Guarda el nuevo equipo en el almacenamiento local
                    equiposGuardados.push(nuevoEquipo);
                    localStorage.setItem("equipos", JSON.stringify(equiposGuardados));


                    


                    alert("Equipo creado con éxito.");
                    window.location.href = "panel_control.html";
                }
            }
        });



// Paso 2: Verificar si ya existe un archivo de usuarios en localStorage
var usuariosGuardados = localStorage.getItem('usuarios');

// Comprobar si hay datos de usuarios en localStorage y parsearlos a un objeto JavaScript
if (usuariosGuardados) {
    usuarios = JSON.parse(usuariosGuardados);
}
else{

    alert("No hay usuarios registrados");
    
}

//buscar un ususrio que tenga el estado en 1
var usuarioExistente = usuarios.find(function (usuario) {
    return (
        usuario.estado === 1
    );
});

if (usuarioExistente) {
    var tablaEquiposBody_2 = document.getElementById("barra");
    var fila_1 = document.createElement("div");

    fila_1.innerHTML = `
        <label class=""> Bienvenido ${usuarioExistente.nombre_1} </label>

        <div class="d-grid justify-content-end ">
            <button type="button" name="" id="Cerrar_Seccion" class="btn btn-dark justify-content-end">Cerrar Seccion</button>
        </div>`;

    tablaEquiposBody_2.appendChild(fila_1); 
} else {
    alert("Usuario no registrado o contraseña incorrecta.");
}

$(Cerrar_Seccion).click(function (e) { 
    e.preventDefault();


    $(document).on('click', '#Cerrar_Seccion', function () {
        // Cambiar el estado del usuario de 1 a 0
        cambiarEstadoUsuario(usuarioExistente.correo, 0);
    
        // Redirigir a la página de inicio de sesión
        window.location.href = "Login.html";
    });
    
   
});

 // Función para cambiar el estado del usuario
 function cambiarEstadoUsuario(idUsuario, nuevoEstado) {
    var usuariosGuardados = JSON.parse(localStorage.getItem('usuarios')) || [];

    for (var i = 0; i < usuariosGuardados.length; i++) {
        if (usuariosGuardados[i].correo === idUsuario) {
            usuariosGuardados[i].estado = nuevoEstado;
            break; // Termina el bucle una vez encontrado el usuario
        }
    }

    // Guarda los usuarios actualizados de vuelta en localStorage
    localStorage.setItem('usuarios', JSON.stringify(usuariosGuardados));
}






    });

