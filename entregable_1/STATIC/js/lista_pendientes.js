$(document).ready(function () {
    // Llama a la función para cargar los equipos pendientes cuando se carga la página
    cargarEquiposPendientes();
});

// Función para calcular la próxima rutina sumando 30 días a la fecha de la última rutina
function calcularProximaRutina(fechaUltimaRutina) {
    // Verifica si la fechaUltimaRutina es un valor válido
    if (!fechaUltimaRutina || isNaN(Date.parse(fechaUltimaRutina))) {
        return "Fecha inválida";
    }

    const fecha = new Date(fechaUltimaRutina);
    fecha.setDate(fecha.getDate() + 30);
    return fecha.toISOString().split('T')[0]; // Formato yyyy-mm-dd
}

// Función para cargar los equipos pendientes en la tabla
function cargarEquiposPendientes() {
    var equiposGuardados = JSON.parse(localStorage.getItem("equipos")) || [];

    // Filtrar equipos con estado "pendiente"
    var equiposPendientes = equiposGuardados.filter(function (equipo) {
        return equipo.estado === "pendiente" || equipo.estado === "Seleccione una";
    });

    var tablaEquiposBody = document.getElementById("equiposTableBody");

    equiposPendientes.forEach(function (equipo) {
        var fila = document.createElement("tr");
        if (equipo.estado === "Seleccione una") {
            equipo.estado = "pendiente";
        }
        fila.innerHTML = `
            <td>${equipo.nombreCliente}</td>
            <td>${equipo.ubicacion}</td>
            <td>${equipo.serial}</td>
            <td>${equipo.tipoEquipo}</td>
            <td>${equipo.estado}</td>
            <td>${equipo.fechaInstalacion}</td>
            <td>${calcularProximaRutina(equipo.fechaInstalacion)}</td>
            <td>
                <div class="form-group">
                    <select class="form-select form-select-sm" name="" id="E_${equipo.serial}">
                        <option selected>Seleccione una</option>
                        <option value="Preventivo">Preventivo</option>
                        <option value="Correctivo">Correctivo</option>
                        <option value="Llamada de emergencia">Llamada de emergencia</option>
                    </select>
                </div>
            </td>
            <td><button type="button" name="" id="${equipo.serial}" class="btn btn-warning">Aplicar</button></td>
        `;
        tablaEquiposBody.appendChild(fila);
    });
}


// Agrega un evento click a los botones "Aplicar"
$(document).on('click', '.btn-warning', function () {
    // Obtén el ID del botón clickeado (que coincide con el serial del equipo)
    var equipoSerial = $(this).attr('id');
    
    // Obtén el nuevo estado desde la columna de acciones
    var nuevoEstado = $('#E_' + equipoSerial).val();

    // Actualiza el estado del equipo en el JSON almacenado en localStorage
    actualizarEstadoEquipo(equipoSerial, nuevoEstado);
    //refrescar pantalla
    location.reload();

    
});




    

    

// Función para actualizar el estado del equipo en el JSON almacenado en localStorage
function actualizarEstadoEquipo(serial, nuevoEstado) {
    var equiposGuardados = JSON.parse(localStorage.getItem("equipos")) || [];

    for (var i = 0; i < equiposGuardados.length; i++) {
        if (equiposGuardados[i].serial === serial) {
            equiposGuardados[i].estado = nuevoEstado;
            break; // Termina el bucle una vez encontrado el equipo
        }
    }

    // Guarda los equipos actualizados de vuelta en localStorage
    localStorage.setItem("equipos", JSON.stringify(equiposGuardados));
}








// Paso 2: Verificar si ya existe un archivo de usuarios en localStorage
var usuariosGuardados = localStorage.getItem('usuarios');

// Comprobar si hay datos de usuarios en localStorage y parsearlos a un objeto JavaScript
if (usuariosGuardados) {
    usuarios = JSON.parse(usuariosGuardados);
}
else{

    alert("No hay usuarios registrados");
    
}

//buscar un ususrio que tenga el estado en 1
var usuarioExistente = usuarios.find(function (usuario) {
    return (
        usuario.estado === 1
    );
});

if (usuarioExistente) {
    var tablaEquiposBody_2 = document.getElementById("barra");
    var fila_1 = document.createElement("div");

    fila_1.innerHTML = `
        <label class=""> Bienvenido ${usuarioExistente.nombre_1} </label>

        <div class="d-grid justify-content-end ">
            <button type="button" name="" id="Cerrar_Seccion" class="btn btn-dark justify-content-end">Cerrar Seccion</button>
        </div>`;

    tablaEquiposBody_2.appendChild(fila_1); 
} else {
    alert("Usuario no registrado o contraseña incorrecta.");
}










$(Cerrar_Seccion).click(function (e) { 
    e.preventDefault();


    $(document).on('click', '#Cerrar_Seccion', function () {
        // Cambiar el estado del usuario de 1 a 0
        cambiarEstadoUsuario(usuarioExistente.correo, 0);
    
        // Redirigir a la página de inicio de sesión
        window.location.href = "Login.html";
    });
    
   
});


 // Función para cambiar el estado del usuario
 function cambiarEstadoUsuario(idUsuario, nuevoEstado) {
    var usuariosGuardados = JSON.parse(localStorage.getItem('usuarios')) || [];

    for (var i = 0; i < usuariosGuardados.length; i++) {
        if (usuariosGuardados[i].correo === idUsuario) {
            usuariosGuardados[i].estado = nuevoEstado;
            break; // Termina el bucle una vez encontrado el usuario
        }
    }

    // Guarda los usuarios actualizados de vuelta en localStorage
    localStorage.setItem('usuarios', JSON.stringify(usuariosGuardados));
}






