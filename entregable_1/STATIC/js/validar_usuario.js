$(entrar).click(function (e) {
    e.preventDefault();

    // Obtener los valores del formulario
    var correoLogin = $("#email").val();
    var contraseñaLogin = $("#Contraseña").val();

    // Obtener usuarios guardados desde el localStorage
    var usuariosGuardados = localStorage.getItem('usuarios');

    // Comprobar si hay datos de usuarios en localStorage y parsearlos a un objeto JavaScript
    if (usuariosGuardados) {
        var usuarios = JSON.parse(usuariosGuardados);

        // Buscar al usuario con el correo y contraseña ingresados
        var usuarioExistente = usuarios.find(function (usuario) {
            return (
                usuario.correo === correoLogin &&
                usuario.contraseña === contraseñaLogin
            );
        });

        if (usuarioExistente) {
            alert("¡Bienvenido, " + usuarioExistente.nombre_1 + "!");

            // Cambiar el estado del usuario a 1
            usuarioExistente.estado = 1;

            // Actualizar los datos en localStorage
            localStorage.setItem('usuarios', JSON.stringify(usuarios));

            // Redirigir al panel de control
            window.location.href = "panel_control.html";
        } else {
            alert("Usuario no registrado o contraseña incorrecta.");
        }
    } else {
        alert("No hay usuarios registrados");
    }
});
