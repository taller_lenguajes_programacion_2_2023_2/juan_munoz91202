import pandas as pd
import sqlite3
import json
import re
import os

from inmueble import Inmueble

class Casa(Inmueble):
      

    def __init__(self, Id=0, ubicacion="", area="", habitaciones="", baños="", precio="", jardin="", piscina="", patio="", garaje="", pisos="", año=""):
            
        super().__init__(Id, ubicacion, area, habitaciones, baños, precio)                

        self.__Jardin = jardin
        self.__garaje = garaje
        self.__Piscina = piscina
        self.__Patio = patio        
        self.__pisos = pisos
        self.__año = año

        print("Se ha creado un objeto Inmueble")
        ruta = "C:/Users/juano/OneDrive/Escritorio/Taller_progrmacion_2/juan_munoz91202/entregable_2/static/xlsx/datos_inmuebles.xlsx"
        #print("esta es la ruta: ", ruta)
        self.df = pd.read_excel(ruta ,sheet_name="casa")
        #print(self.df)
        #print("se ha cargado el archivo excel")
        

        self.create_casa( nom_tabla="casa",datos_tbl="carga_casa" )
        
        self.insert_casa( nom_tabla="casa",nom_columns="carga_casa")

    @property
    def _Jardin(self):
        return self.__Jardin
    @_Jardin.setter
    def _Jardin(self, jardin):        
        self.__Jardin = jardin
        print("Jardin {} modificado con éxito" .format (self.__Jardin))


    @property
    def _Piscina(self):
        return self.__Piscina

    @_Piscina.setter
    def _Piscina(self, piscina):        
        self.__Piscina = piscina
        print("Piscina {} modificado con éxito" .format (self.__Piscina))


    @property
    def _Patio(self):
        return self.__Patio

    @_Patio.setter
    def _Patio(self, patio):        
        self.__Patio = patio
        print("Patio {} modificado con éxito" .format (self.__Patio))


    @property
    def _garaje(self):
        return self.__garaje

    @_garaje.setter
    def _garaje(self, garaje):        
        self.__garaje = garaje
        print("garaje {} modificado con éxito" .format (self.__garaje))



    @property
    def _pisos(self):
        return self.__pisos

    @_pisos.setter
    def _pisos(self, pisos):        
        self.__pisos = pisos
        print("pisos {} modificado con éxito" .format (self.__pisos))


    @property
    def _año(self):
        return self.__año

    @_año.setter
    def _año(self, año):        
        self.__año = año
        print("año {} modificado con éxito" .format (self.__año))





    def insert_casa(self,nom_tabla="",nom_columns=""):
            datos=""
            for index, row in self.df.iterrows():
                datos = '{},"{}",{},{},{},{},{},{},"{}",{},{},{}'.format(row["ID"],row["direccion"],row["Area"],row["Habitaciones"],row["banos"],row["Precio"],row["jardin"],row["Garaje"],row["Piscina"],row["patio"],row["pisos"],row["creacion"])
                self.insertar_datos(nom_tabla= nom_tabla,nom_columns= nom_columns,datos_carga=datos)
                #print (datos)
            return True

    def create_casa(self, nom_tabla="",datos_tbl=""):
        atributos = vars(self)
        if self.crear_tabla(nom_tabla=nom_tabla,datos_tbl=datos_tbl):
            print("Tabla casa Creada!!!")
        
        return atributos  


    def __str__(self) :
        """funcion para visualizar objeto persona
        Args:
            no tiene parametros 
        """
        return '{},"{}",{},{},{},{},{},{},{},"{}",{},{}'.format(self._Id, self._ubicacion, self._area, self._habitaciones, self._baños, self._precio, self._Jardin, self._garaje, self._Piscina, self._Patio, self._pisos, self._año)