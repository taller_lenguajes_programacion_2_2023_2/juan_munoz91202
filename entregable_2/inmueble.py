import pandas as pd
import sqlite3
import json
import re
import os

from conexion import Conexion






class Inmueble(Conexion):

    def __init__(self, Id=0, ubicacion="", area="", habitaciones="", baños="", precio=""):
        self.__Id = Id
        self.__ubicacion = ubicacion
        self.__area = area
        self.__habitaciones = habitaciones
        self.__baños = baños
        self.__precio = precio
        super().__init__()

        
        ruta = "C:/Users/juano/OneDrive/Escritorio/Taller_progrmacion_2/juan_munoz91202/entregable_2/static/xlsx/datos_inmuebles.xlsx"
        #print("esta es la ruta: ", ruta)
        self.df = pd.read_excel(ruta ,sheet_name="inmueble")
        #print(self.df)
        #print("se ha cargado el archivo excel")
        self.create_Inmueble( nom_tabla="inmueble",datos_tbl="carga_inumeble" )
        self.insert_Inmueble( nom_tabla="inmueble",nom_columns="carga_inumeble")
        
        
    
        

    @property
    def _Id(self):
        return self.__Id

    @_Id.setter
    def _Id(self, Id):        
        self.__Id = Id
        print("ID {} modificado con éxito" .format (self.__Id))

    @property
    def _ubicacion(self):
        return self.__ubicacion

    @_ubicacion.setter
    def _ubicacion(self, ubicacion):        
        self.__ubicacion = ubicacion
        print("ubicacion {} modificado con éxito" .format (self.__ubicacion))

    @property
    def _area(self):
        return self.__area

    @_area.setter
    def _area(self, area):        
        self.__area = area
        print("area {} modificado con éxito" .format (self.__area))


    @property
    def _habitaciones(self):
        return self.__habitaciones

    @_habitaciones.setter
    def _habitaciones(self, habitaciones):        
        self.__habitaciones = habitaciones
        print("habitaciones {} modificado con éxito" .format (self.__habitaciones))



    @property
    def _baños(self):
        return self.__baños

    @_baños.setter
    def _baños(self, baños):        
        self.__baños = baños
        print("baños {} modificado con éxito" .format (self.__baños))


    @property
    def _precio(self):
        return self.__precio

    @_precio.setter
    def _precio(self, precio):        
        self.__precio = precio
        print("precio {} modificado con éxito" .format (self.__precio))

    def insert_Inmueble(self,nom_tabla="",nom_columns=""):
            datos=""
            for index, row in self.df.iterrows():
                datos = '{},"{}",{},{},{},{}'.format(row["ID"],row["direccion"],row["Area"],row["Habitaciones"],row["banos"],row["Precio"])
                self.insertar_datos(nom_tabla= nom_tabla, nom_columns= nom_columns, datos_carga=datos)
                #print (datos)
            return True
    
    def create_Inmueble(self, nom_tabla="",datos_tbl=""):
        atributos = vars(self)
        if self.crear_tabla(nom_tabla=nom_tabla,datos_tbl=datos_tbl):
            print("Tabla inmueble Creada!!!")
        
        return atributos


def __str__(self) :
    """funcion para visualizar objeto persona
    Args:
        no tiene parametros 
    """
    return '{},"{}",{},{},{},{}'.format(self._Id, self._ubicacion, self._area, self._habitaciones, self._baños, self._precio)

                        


   