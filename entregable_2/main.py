from inmueble import Inmueble
from casa import Casa

def main():

   
   
   #Inmueble1 = Inmueble(1, "Calle 1", 100, 3, 2, 100000)
   
   casa1=Casa(13, "Calle 1", 100, 3, 2, 100000, 50, 2, "Si", 35, 2, 2021) 
   
   d = casa1.leer_dato("casa")
   print(d)


   print("_________________________________________________________") 
   print("_________________________________________________________")
   print(" se inserta un objeto adicional a la tabla casa")
   print("_________________________________________________________")
   print("_________________________________________________________")
   casa1.insertar_datos( nom_tabla="casa",nom_columns="carga_casa",datos_carga=casa1)
   print("_________________________________________________________") 
   print("_________________________________________________________")
   print("se lee la tabla casa")
   print("_________________________________________________________")
   print("_________________________________________________________")
   d = casa1.leer_dato("casa")
   print(d)  

   print("_________________________________________________________") 
   print("_________________________________________________________")
   print("se elimina el objeto con id 1,2,3,4,5,6 de la tabla casa")
   print("_________________________________________________________")
   print("_________________________________________________________")
   casa1.eliminar_dato_por_id("casa", 1)
   casa1.eliminar_dato_por_id("casa", 2)
   casa1.eliminar_dato_por_id("casa", 3)
   casa1.eliminar_dato_por_id("casa", 4)
   casa1.eliminar_dato_por_id("casa", 5)
   casa1.eliminar_dato_por_id("casa", 6)

   print("_________________________________________________________") 
   print("_________________________________________________________")
   print("se imprime la base de datos luego de la eliminacion de los objetos con id 1,2,3,4,5,6 de la tabla casa")
   print("_________________________________________________________")
   print("_________________________________________________________")

   d = casa1.leer_dato("casa")
   print(d)


   print("_________________________________________________________") 
   print("_________________________________________________________")
   print("imprimo el dato de la fila 7 y modifico varios parametros, luego imprimo el dato de la fila 7")
   print("_________________________________________________________")
   print("_________________________________________________________")

   print(casa1.leer_dato_por_id("casa", 7))
   casa1.modificar_dato_por_id("casa", 7, "direccion='Calle 23323'")
   casa1.modificar_dato_por_id("casa", 7, "Area=100")
   casa1.modificar_dato_por_id("casa", 7, "Habitaciones=3")
   casa1.modificar_dato_por_id("casa", 7, "banos=2")
   casa1.modificar_dato_por_id("casa", 7, "Precio=100000")   
   print(casa1.leer_dato_por_id("casa", 7))
   d = casa1.leer_dato("casa")
   print(d)
  
   

   
   
 
   

  

   
   
 
   

   

   
if __name__ == '__main__':
    main()
