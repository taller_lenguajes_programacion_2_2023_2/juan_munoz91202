import sqlite3
import json
import re

class Conexion:
    def __init__(self) -> None:
        self.__user=""
        self.__password=""
        self.__puerto=0
        self.__url=""
        __nom_db="db_afcj_prueba.sqlite"
        self.__querys = self.obtener_json()
        self.__conex = sqlite3.connect(__nom_db)
        self.__cursor = self.__conex.cursor()
        #conex = sqlite3.connect()
    
    def obtener_json(self):
        #E:/taller_programacion2/andres_callejas05082/src/poli_proyecto/static/sql/querys.json
        ruta = "C:/Users/juano/OneDrive/Escritorio/Taller_progrmacion_2/juan_munoz91202/entregable_2/static/sql/querys.json"
        querys={}
        with open(ruta, 'r') as file:
            querys = json.load(file) 
        return querys
    
    def crear_tabla(self, nom_tabla="",datos_tbl = ""):
        if nom_tabla != "" :
            datos=self.__querys[datos_tbl]
            query =  self.__querys["create_tb"].format(nom_tabla,datos)
            self.__cursor.execute(query)
            #print(query)
            return True
        else:
            return False
        
    def insertar_datos(self, nom_tabla="",nom_columns = "", datos_carga=""):
        if nom_tabla != "" :
            columnas=self.__querys[nom_columns]
            query =  self.__querys["insert"].format(nom_tabla,columnas,datos_carga)
            self.__cursor.execute(query)
            self.__conex.commit()
            #print(query)
            return True
        else:
            return False
        
    def leer_dato(self, nom_tabla=""):
        if nom_tabla != "":
            query = self.__querys["select"].format(nom_tabla)
            self.__cursor.execute(query)
            result = self.__cursor.fetchall()
            return result
        else:
            return None    

    def eliminar_dato_por_id(self, nom_tabla="", id_valor=0):
        if nom_tabla != "" and id_valor != 0:
            condicion = "id = {}".format(id_valor)
            query = self.__querys["delete"].format(nom_tabla, condicion)
            try:
                self.__cursor.execute(query)
                self.__conex.commit()
                return print("Registro con ID {} eliminado correctamente.".format(id_valor))
            except sqlite3.Error as e:
                return print("Error al eliminar el registro: {}".format(str(e)))
        else:
            return print("Debe proporcionar el nombre de la tabla y un ID válido.")
        
        

    def leer_dato_por_id(self, nom_tabla="", id_valor=0):
        if nom_tabla != "" and id_valor != 0:
            condicion = "id = {}".format(id_valor)
            query = self.__querys["select"].format(nom_tabla) + " WHERE {}".format(condicion)
            try:
                self.__cursor.execute(query)
                result = self.__cursor.fetchone()
                return result
            except sqlite3.Error as e:
                return "Error al leer el dato con ID {} de la tabla '{}': {}".format(id_valor, nom_tabla, str(e))
        else:
            return "Debe proporcionar el nombre de la tabla y un ID válido."
        
        

    def modificar_dato_por_id(self, nom_tabla="", id_valor=0, nuevos_datos=""):
        """
        Modifica los datos de un registro en la tabla especificada por su ID.
        """
        if nom_tabla != "" and id_valor != 0 and nuevos_datos:
            # Crear la condición para seleccionar el registro por su ID
            condicion = "id = {}".format(id_valor)

            # Crear la consulta para actualizar los datos
            query = self.__querys["update"].format(nom_tabla, nuevos_datos, condicion)

            try:
                self.__cursor.execute(query)
                self.__conex.commit()
                return print("Registro con ID {} modificado correctamente.".format(id_valor))
            except sqlite3.Error as e:
                return print("Error al modificar el registro: {}".format(str(e)))
        else:
            return print("Debe proporcionar el nombre de la tabla, un ID válido y los nuevos datos.")
    
    


  