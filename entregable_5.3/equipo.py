from conexion import Conexion

class Equipo (Conexion):
    def __init__(self, id="", tipo="", sitio="", voltaje="", num_compresores="", num_blower="", num_vent="", observaciones=""):
        self.__id = id
        self.__idtipo = tipo
        self.__sitio = sitio
        self.__voltaje = voltaje
        self.__num_compresores = num_compresores
        self.__num_blower = num_blower
        self.__num_vent = num_vent
        self.__observaciones = observaciones

        super().__init__()
        self.create_Equ()


    def create_Equ(self):
         # atributos = vars(self)
        if self.crear_tabla(nom_tabla="equipo", datos_tbl="datos_equ"):
            print("Tabla productos creada")
        return True
    
    def __str__(self):
        return "Equipo: id {} tipo {}".format(self.id, self.tipo)
