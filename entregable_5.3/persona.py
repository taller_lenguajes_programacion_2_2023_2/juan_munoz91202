import pandas as pd
from conexion import Conexion

class Persona (Conexion):
    def __init__(self, id=0, primer_nombre="", segundo_nombre="", primer_apellido="", segundo_apellido="", correo_electronico="", contraseña="", rol=""):
        self.__id = id
        self.__primer_nombre = primer_nombre
        self.__segundo_nombre = segundo_nombre
        self.__primer_apellido = primer_apellido
        self.__segundo_apellido = segundo_apellido
        self.__correo_electronico = correo_electronico
        self.__contraseña = contraseña
        self.__rol = rol
        super().__init__()
        self.create_Per()

        ruta = "juan_munoz91202/entregable_2/static/xlsx/datos_proyecto.xlsx"
        self.df = pd.read_excel(ruta, sheet_name="persona")
        self.insert_Per()


        @property
        def _id(self):
            return self.__id

        @_id.setter
        def id(self, value):
            self.__id = value

    def create_Per(self):
        atributos = vars(self)
        if self.crear_tabla(nom_tabla="persona", datos_tbl="datos_per"):
            print("Tabla persona creada")
        return atributos

    def insert_Per(self):
        datos = ""
        for index, row in self.df.iterrows():
            datos = '{},"{}","{}","{}","{}","{}","{}","{}"'.format(row['id'], row['primer_nombre'], row['segundo_nombre'], row['primer_apellido'], row['segundo_apellido'], row['correo_electronico'], row['contraseña'], row['rol'])
            self.insertar_datos(nom_tabla="persona", nom_columnas="carga_per", datos_carga=datos)
        return True

    def update_Per(self,id=0):
        return True

    def delete_Per(self,id=0):
        return True

    def select_Per(self,id=0):
        return True

    def __str__(self):
        return "Persona: id {} nombre {}".format(self.id, self.primer_nombre)
