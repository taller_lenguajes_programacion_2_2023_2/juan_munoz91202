from persona import Persona
from equipo import Equipo
from conexion import Conexion

class Mantenimiento(Conexion):
    def __init__(self, id=0, tipo_mtto="", fecha_eje="27/09/2023"):
        self.__id = id
        self.__tipo_mtto = tipo_mtto
        self.__fecha_eje = fecha_eje
        self.__usuario = Persona()
        self.__equipo = Equipo()
        super().__init__()
        self.create_Mtto()



    def create_Mtto(self):
         # atributos = vars(self)
        if self.crear_tabla(nom_tabla="mantenimiento", datos_tbl="datos_mtto"):
            print("Tabla mantenimiento creada")
        return True


    def __str__(self):
        return "Mantenimiento: id {} tipo_mtto {} usuario {} equipo {}".format(self.id, self.tipo_mtto, self.usuario, self.equipo)
